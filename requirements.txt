ipykernel==5.3.4
ipython==7.18.1
ipython-genutils==0.2.0
jupyter==1.0.0
jupyter-client==6.1.7
jupyter-console==6.2.0
jupyter-core==4.6.3
jupyterlab-pygments==0.1.2
matplotlib==3.3.3
networkx==2.5
numpy==1.19.4
scipy==1.5.2

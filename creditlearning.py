import numpy as np
from scipy.special import factorial
import matplotlib.pyplot as plt
import networkx as nx

def bounded_exp(x,bound=710,maxval=700):
    """ Helper function that cuts off exponential if argument is too large. Input: n-D array."""
    x[np.where(x>=bound)] = maxval
    return np.exp(x)

def bounded_log(x,bound=1e-300,minval=1e-300):
    """ Helper function that cuts off log if argument is 0. Input: n-D array."""
    x[np.where(x<=bound)] = minval
    return np.log(x)

class CreditLearner:
    """
    Learns the credit tensor C for a temporal function f of stimuli using leaky integrator memory.
    The input function f must be a 2D array with dimensions (no. of stimuli x no. of timesteps),
    where the (i,n)-th entry is the function for stimulus i evaluated at time n*dt.

    Class contains:
    -- F : (num. stimuli x num. taustars), Laplace transform of each stimulus' history
    -- ftilde: (num. stimuli x num. taustars), approximate inverse LT of F
    -- M : (num. taustars x num. stimuli x num. stimuli), associative memory
    -- expC : (num. taustars x num. stimuli x num. stimuli), exponentiated credit density
    """

    def __init__(self,tau_min=0.1,tau_max=10,num_taustars=50,
                 delta_min=None,delta_max=None,k=8,dt=0.001,g=0,
                 num_stimuli=1,lrM=1,lrC=1,gradientClip=100,convM=True,
                 counts=None,appearances=None,initialC="uniform",initialpbase=None,stimuli_labels=None):
        """
        Inputs:
        tau_min : float, minimum taustar in memory representation
        tau_max : float, maximum taustar in memory representation
        num_taustars : int, number of nodes desired in memory representation
        delta_min : float, lower bound on minimum delta in C
        delta_max : float, upper bound on maximum delta in C
        k : int, controls accuracy of inverse laplace transform
        dt : float, timestep used in Euler method integration
        g : 0 for power law scaling of f tilde, 1 for equal amplitude
        convM : bool, set to True to use convolutional M, else uses regular normalized M
        num_stimuli : int, number of stimuli
        lrM : float, learning rate for M in units of dt
        lrC : float, learning rate for C in units of dt (i.e. maximum can be 1/dt)
        gradientClip: float, maximum relative change in C allowed when a stimulus is presented
        counts : int, d=no.stimuli, initial stimuli counts
        appearances : int, time interval over which initial counts have been collected
        initialC : float, d=no.taustar x no.stimuli x no.stimuli, initial exponentiated credit density
        initialpbase : float, d=no.stimuli, initial probabilities of appearance
        stimuli_labels : string, d=no. stimuli, names of stimuli
        """

        # Register inputs
        self.tau_min = tau_min
        self.tau_max = tau_max
        self.num_taustars = num_taustars
        self.convM = convM
        self.delta_min = tau_min if delta_min is None or not self.convM else delta_min
        self.delta_max = tau_max if delta_max is None or not self.convM else delta_max
        self.k = k
        self.dt = dt
        self.g = g
        self.num_stimuli = num_stimuli
        self.lrM = lrM
        assert(lrC<=1/dt) #learning rate is in units of dt
        self.lrC = lrC
        self.gradientClip = gradientClip

        self.num_nodes = self.num_taustars + 2*self.k #add 2k padding nodes for derivative
        self.dtau = np.exp((np.log(self.tau_max)-np.log(self.tau_min))/(self.num_taustars-1))
        self.taustars = self.tau_min*self.dtau**(np.arange(self.num_nodes)-self.k)
        self.s = self.k/self.taustars[:]

        self.delta_start = np.argwhere(self.taustars>=self.delta_min)[0,0]
        self.delta_end = np.argwhere(self.taustars<=self.delta_max)[-1,0]
        self.num_deltas = self.delta_end - self.delta_start
        self.deltas = self.taustars[self.delta_start:self.delta_end]

        if self.convM:
            h = sum(1/n for n in range(1,self.k))
            gamma = 0.5772156649015328606065120
            self.kappa = self.k**(self.k+1)*np.exp(-(self.k+1)*(h-gamma))
            assert(self.num_deltas<=self.num_taustars)
        else:
            assert(self.num_taustars==self.num_deltas)

        # Integration measures
        self.taustarmeasure = self.taustars[self.k+1:-self.k+1]-self.taustars[self.k:-self.k]
        self.deltameasure = self.taustars[self.delta_start+1:self.delta_end+1]-self.taustars[self.delta_start:self.delta_end]

        # Stimuli statistics
        eps = 1e-20
        self.counts = eps*np.ones(self.num_stimuli) if counts is None else counts
        self.appearances = eps if appearances is None else appearances
        self.pbase = None

        # Representations of F, ftilde, M, C
        self.F = np.zeros((self.num_nodes,self.num_stimuli))
        self.ftilde = np.zeros((self.num_taustars,self.num_stimuli))

        self.M = np.zeros((self.num_taustars,self.num_stimuli,self.num_stimuli))
        self.Mnorm = np.zeros((self.num_taustars,self.num_stimuli,self.num_stimuli))
        self.Mconv = np.zeros((self.num_deltas,self.num_stimuli,self.num_stimuli)) if self.convM else None

        self.expC = np.ones((self.num_deltas,self.num_stimuli,self.num_stimuli)) if initialC=="uniform" else np.random.normal(1,1e-1,(self.num_deltas,self.num_stimuli,self.num_stimuli))
        self.pminus = np.zeros((self.num_stimuli,self.num_deltas,self.num_stimuli))

        # Derivative operator for inverse laplace transform
        self.Ck = ((-1)**self.k)*(self.s[:]**(self.k+1))*(self.taustars[:]**self.g)/factorial(self.k)
        self.dds = np.zeros((self.num_nodes,self.num_nodes))
        for i in range(1,self.num_nodes-1):
            self.dds[i, i-1] = -(self.s[i+1]-self.s[i])/(self.s[i]-self.s[i-1])/(self.s[i+1] - self.s[i-1])
            self.dds[i, i] = ((self.s[i+1]-self.s[i])/(self.s[i]- self.s[i-1])-(self.s[i]-self.s[i-1])/(self.s[i+1]-self.s[i]))/(self.s[i+1] - self.s[i-1])
            self.dds[i, i+1] = (self.s[i]-self.s[i-1])/(self.s[i+1]-self.s[i])/(self.s[i+1] - self.s[i-1])
        self.dkdsk = np.linalg.matrix_power(self.dds, self.k)

        # Stimuli Labels
        if stimuli_labels is not None and len(stimuli_labels)>=self.num_stimuli:
            self.stimuli_labels = stimuli_labels
        else:
            self.stimuli_labels = np.arange(self.num_stimuli).astype(str)

        # Single stimulus projections used in propagateexpC function

        self.unit_ftildeplus_delta = bounded_exp(-self.s[:,np.newaxis]*self.deltas[np.newaxis,:])
        self.unit_ftildeplus_delta = self.dkdsk.dot(self.unit_ftildeplus_delta)
        self.unit_ftildeplus_delta = (self.Ck[:,np.newaxis]*self.unit_ftildeplus_delta)[self.k:-self.k,:]
        self.unit_ftildeplus_delta[self.unit_ftildeplus_delta<0] = 1e-20
        self.unit_ftildeplus_delta_womeasure = self.unit_ftildeplus_delta.copy()
        self.unit_ftildeplus_delta *= self.taustarmeasure[:,np.newaxis]


    # Data Alteration
    def resetC(self,expC=None):
        "Initializes expC with some input tensor. If none, initializes with all 1's"
        if expC is None:
            self.expC = np.ones((self.num_deltas,self.num_stimuli,self.num_stimuli))
        else:
            assert(expC.shape==(self.num_deltas,self.num_stimuli,self.num_stimuli))
            self.expC =  expC

    def resetF(self,F=None):
        "Initializes F with some input vector. If none, initializes with all 0's"
        if F is None:
            self.F = np.zeros((self.num_nodes,self.num_stimuli))
        else:
            assert(F.shape==(self.num_nodes,self.num_stimuli))
            self.F = F

    def resetftilde(self,ftilde=None):
        "Initializes ftilde with some input vector. If none, initializes with all 0's"
        if ftilde is None:
            self.ftilde = np.zeros((self.num_taustars,self.num_stimuli))
        else:
            assert(ftilde.shape==np.zeros((self.num_taustars,self.num_stimuli,)))
            self.ftilde = ftilde

    def resetCounts(self,counts=None,eps=1e-20):
        self.appearances = eps
        if counts is None:
            self.counts = eps*np.ones(self.num_stimuli)
        else:
            assert(counts.shape==(self.num_stimuli,))
            self.counts = counts

    def resetpbase(self,pbase):
        assert(len(pbase)==self.num_stimuli)
        self.pbase = pbase

    # Data retrival
    def getTaustars(self,start=None,end=None):
        if start is None: start = self.k
        if end is None: end = -self.k
        return self.taustars[start:end]

    def getDeltas(self,start=None,end=None):
        if start is None: start = 0
        if end is None: end = len(self.deltas)
        return self.deltas[start:end]

    def getM(self,norm="n"):
        """
        Inputs:
        norm : none --> non-normalized, count --> count-normalized
        """
        assert(norm in ["none","count"])
        if norm=="none":
            return self.M
        elif norm=="count":
            return self.Mnorm

    # Data dynamics and learning
    def prepareInputFunc(self,totaltime,func,deltas=False):
        """
        Given a function f : t --> R^no.stimuli, constructs 2D array used for learning C.
        Inputs:
        totaltime : float, total time of experiment
        func : function, input = time, float and output = float/int, 1D array with dim=no.stimuli
        deltas : if true, treats all non-zero elements as delta functions
        """

        timesteps = int(totaltime/self.dt)
        f = np.zeros((self.num_stimuli,timesteps))

        for n in timesteps:
            f[:,n] = func(n*self.dt)
            if deltas: f[:,n] = (1/self.dt)*(f[:,n]!=0)

        return f

    def propagateF(self,f=None,time=None):
        """ Propagates leaky integrators in time
        Inputs:
        f : 1D array, d=no. stimuli, stimulus function evaluated at present
        time : float, if no inputs present, uses analytical formula to propagate forward in time
        """

        if f is None: f = np.zeros(self.num_stimuli)

        if time is None:
            self.F += self.dt*(-self.s[:,np.newaxis]*self.F + f[np.newaxis,:])
        else:
            self.F *= bounded_exp(-time*self.s[:,np.newaxis])

    def invertF(self):
        """ Construct and store ftilde using current state of leaky integrators """

        self.ftilde = (self.Ck[:,np.newaxis]*self.dkdsk.dot(self.F))[self.k:-self.k,:]
        self.ftilde[self.ftilde<0] = 1e-20

    def updateMnorm(self,eps=1e-20):
        """ Compute the normalized version of M """
        self.Mnorm = self.M/(self.counts[np.newaxis,:,np.newaxis])

    def updateMconv(self,eps=1e-20):
        """Compute convolutional M"""

        temp = np.einsum('tij,td->tdij',bounded_log(self.Mnorm),self.unit_ftildeplus_delta_womeasure)
        self.Mconv = self.kappa*bounded_exp(np.trapz(y=temp,x=self.taustars[self.k:-self.k],axis=0))
        #self.Mconv = self.kappa*bounded_exp(np.einsum('tij,td->dij',bounded_log(self.Mnorm),self.unit_ftildeplus_delta))

    def propagateM(self,f=None,updateMconv=True):
        """ Propagates M in time by dt
        Inputs:
        f : 1D array, d=no. stimuli, stimulus function evaluated at present
        """
        if f is None:
            f = np.zeros(self.num_stimuli)

        if np.any(f!=0):
            self.M += self.lrM*self.dt*self.ftilde[:,:,np.newaxis]*f[np.newaxis,np.newaxis,:]
            self.M[self.M<0] = 1e-20

        self.updateMnorm()

        if self.convM and updateMconv:
            self.updateMconv()

    def translateftilde(self,delta,F=None):
        """ Construct and return delta-translated ftilde using current state of leaky integrators """

        if F is None:
            F = self.F

        shiftedftilde = (self.Ck[:,np.newaxis]*self.dkdsk.dot(bounded_exp(-self.s*delta)[:,np.newaxis]*F))[self.k:-self.k,:]
        shiftedftilde[shiftedftilde<0] = 1e-20

        return shiftedftilde


    def propagateexpC(self,f=None,eps=1e-20,adaptlr=False):
        """
        Propagates C by timestep dt given the present state of the stimulus function
        alpha : if adaptive learning rate required, then set to float between 0 and 1
        """
        if f is None:
            f = np.zeros(self.num_stimuli)

        stimuli_presented = np.where(f!=0)[0]
        num_stimuli_presented = len(stimuli_presented)

        if num_stimuli_presented>0:

            prefactor = self.counts/self.appearances if self.pbase is None else self.pbase
            ftilde_delta = bounded_exp(-self.s[:,np.newaxis,np.newaxis]*self.deltas[np.newaxis,np.newaxis,:])*self.F[:,:,np.newaxis]
            ftilde_delta = np.einsum('ts,sid->tid',self.dkdsk,ftilde_delta)
            ftilde_delta = (self.Ck[:,np.newaxis,np.newaxis]*ftilde_delta)[self.delta_start:self.delta_end,:,:]
            ftilde_delta[ftilde_delta<0] = 1e-20

            #ftilde_delta *= self.deltameasure[:,np.newaxis,np.newaxis]
            #pminus = prefactor[np.newaxis,:]*bounded_exp(np.einsum('tij,tid->dj',np.log(self.expC),ftilde_delta))
            ftilde_delta = np.einsum('tij,tid->tdj',bounded_log(self.expC+1e-20),ftilde_delta)
            pminus = prefactor[np.newaxis,:]*bounded_exp(np.trapz(y=ftilde_delta,x=self.deltas,axis=0))

            for i in stimuli_presented:

                if adaptlr:
                    alpha = 1/self.counts[i] if self.counts[i]>1 else 1-1e-10
                else:
                    alpha = self.lrC*self.dt

                if self.convM:
                    pplus = self.Mconv[:,i,:]
                else:
                    pplus = self.Mnorm[self.delta_start-self.k:self.delta_end-self.k,i,:]


                if self.counts[i]<1:
                    self.expC[:,i,:] = pplus/(pminus+eps)
                else:
                    expC_grad = alpha*(pplus/(pminus+eps) - self.expC[:,i,:])
                    expC_grad_norm = np.sqrt(np.sum(expC_grad**2))
                    if expC_grad_norm>self.gradientClip:
                        expC_grad = self.gradientClip*expC_grad/expC_grad_norm

                    self.expC[:,i,:] += expC_grad

                    #self.expC[:,i,:] *= (1-alpha)
                    #self.expC[:,i,:] += alpha*(pplus/(pminus+eps))

                self.pminus[i,:,:] = pminus


    def learnCredit(self,f,timesteps=None,burn=-1,
                    cvgcheck=False,rtol=0.001,cvgmintime=0,verbose=0,returnmemory=False,onlyM=False):
        """
        Runs the system on input function f and learns M and C
        Inputs:
        f : 2D array with dim=(num. stimuli,num. timesteps). Interval is assumed
               to be dt, i.e. it is function evaluated at n*dt for n s.t. 0 < n*dt < T.
        timesteps : int, no. of timesteps to run system for. If None, assumes whole input sequence.
        burn : int, timestep after which to reset expC
        cvgcheck : if True, checks for convergence of expC and stops learning if convergence criterion met
        rtol : float, relative tolerance of convergence threshold
        cvgmintime : int, minimum time before convergence can be declared, assumed 0
        propCounts : if True, propagates counts
        verbose : int, if non-zero, will print time after every verbose timesteps
        returnmemory : if True, returns history of F and ftilde in 3D array
        onlyM : if True, learns only M, not expC
        """

        assert(len(f.shape)==2)
        assert(f.shape[0]==num_stimuli)
        if timesteps is None:
            timesteps = f.shape[1]

        if returnmemory:
            storedF = np.zeros((self.num_stimuli,self.num_nodes,timesteps))
            storedftilde = np.zeros((self.num_stimuli,self.num_taustars,timesteps))

        converged = [False for i in range(self.num_stimuli)]

        for t in range(timesteps):

            if verbose>0 and t%verbose==0:
                print("Reached timestep :",t,"/",timesteps)

            if returnmemory:
                storedF[:,:,t] = self.F[:,:]
                storedftilde[:,:,t] = self.ftilde[:,:]

            if t==burn:
                self.resetC()

            if propCounts:
                self.counts += f[:,t]*self.dt
                self.appearances += dt

            if not onlyM:

                if cvgcheck:
                    oldexpC = self.expC[:,:,:]
                    self.propagateexpC(f[:,t])
                    present_events = np.where(f[:,t]>0)[0]

                    for a in present_events:
                        if t>cvgmintime and np.allclose(oldexpC[:,a,:], self.expC[:,a,:], rtol=rtol):
                            converged[a] = True

                    if np.all(converged):
                        break
                else:
                    self.propagateexpC(f[:,t])

                self.propagateM(f[:,t])
            else:
                self.propagateM(f[:,t])

            self.propagateF(f[:,t])
            self.invertF()

        if returnmemory:
            return storedF, storedftilde

    def learnCreditFast(self,f,event_times=None,timesteps=None,
                        store_hist=0,burn=-1,cvgcheck=False,cvgminevents=0,rtol=0.001,
                        propCounts=True,verbose=0,returnmemory=False,
                        suppressC=False,suppressM=False,updateMconv=True,adaptlr=False):
        """
        Runs the system on input function f and learns M and C, optimized for when f is a stream of impulse functions.
        ---------------------------------------------------------------------------------------
        IMPORTANT NOTE : provided dt is not large or small enough to cause numerical errors,
        it makes no difference to the code when using this function. Time propagation of leaky
        integrators is carried out using the analytical formula when no stimulus is present.
        ---------------------------------------------------------------------------------------
        Inputs:
        f : 2D array with dim=(num. stimuli,num. timesteps). Interval is assumed
               to be dt, i.e. it is function evaluated at n*dt for n s.t. 0 < n*dt < T.
               OR
            2D array with dim=(num. stimuli,num. events), i.e. an array of all non-zero stimuli events.
        event_times : 1D array, dim=(num.events,). Array of event times.
        timesteps : int, no. of timesteps to run system for. If None, assumes whole input sequence.
        store_hist : if greater than zero, stores history every store_hist events
        burn : int, timestep after which to reset expC
        cvgcheck : if True, checks for convergence of expC and stops learning if convergence criterion met
        rtol : float, relative tolerance of convergence threshold
        cvgminevents : int, minimum number of events before convergence can be declared
        propCounts : if True, propagates counts and appearances
        verbose : if non-zero, prints statement after every verbose events
        returnmemory : if True, returns history of F and ftilde in 3D array
        suppressC : if True, does not learn expC
        suppressM : if True, does not learn M
        adaptlr : if True, uses adaptive learning rate alpha
        """

        assert(len(f.shape)==2)
        assert(f.shape[0]==self.num_stimuli)

        # Allows for sparse representation of f
        if event_times is not None:
            assert(len(event_times.shape)==1)
            assert(f.shape[1]==event_times.shape[0])
            f_sparse = True
            if timesteps is None: timesteps = event_times[-1]
        else:
            f_sparse = False
            event_times = np.unique(np.where(f>0)[1])
            if timesteps is None:
                timesteps = f.shape[1]
            event_times = np.append(event_times,timesteps-1)
            final_time = f.shape[1]*self.dt

        num_events = event_times.shape[0]

        if returnmemory:
            storedF = np.zeros((self.num_stimuli,self.num_nodes,num_events))
            storedftilde = np.zeros((self.num_stimuli,self.num_taustars,num_events))

        notburnt = True
        converged = [False for i in range(self.num_stimuli)]

        if store_hist>0:
            self.C_history = np.empty((self.num_taustars,self.num_stimuli,self.num_stimuli,1+num_events//store_hist))

        for i in range(num_events):

            if f_sparse:
                ind = i
            else:
                ind = event_times[i]

            time = event_times[i]
            time_gap = event_times[i] - event_times[i-1] if i>0 else event_times[i]

            if time>timesteps: break
            if verbose>0 and i%verbose==0: print("Reached event: ",i,"/",num_events)
            if time>=burn and notburnt:
                self.resetC()
                notburnt = False

            if time_gap>0:
                self.propagateF(time=(time_gap-1)*self.dt)
            self.invertF()

            if propCounts:
                self.counts += f[:,ind]*self.dt
                self.appearances += time_gap*self.dt


            if not suppressC:

                if store_hist>0:
                    if i%store_hist==0:
                        self.C_history[:,:,:,i//store_hist] = self.expC[:,:,:]

                if cvgcheck:
                    oldexpC = self.expC[:,:,:]
                    self.propagateexpC(f[:,ind],adaptlr=adaptlr)
                    present_events = np.where(f[:,ind]>0)[0]

                    for a in present_events:
                        if i>cvgminevents and np.allclose(oldexpC[:,a,:], self.expC[:,a,:], rtol=rtol):
                            converged[a] = True
                        else:
                            converged[a] = False

                    if np.all(converged):
                        break
                else:
                    self.propagateexpC(f[:,ind],adaptlr=adaptlr)

                if not suppressM:
                    self.propagateM(f[:,ind],updateMconv=updateMconv)

            else:
                if not suppressM:
                    self.propagateM(f[:,ind],updateMconv=updateMconv)


            if returnmemory: #Note: stores F right before event presented
                storedF[:,:,i] = self.F[:,:]
                storedftilde[:,:,i] = self.ftilde[:,:]

            self.propagateF(f[:,ind])


        if returnmemory:
            return storedF, storedftilde

    def prelearnM(self,f,event_times=None,andlearnCafter=False,adaptlr=False,verbose=0):
        """ Command to prelearn M. Only works when using the function learnCreditFast.
        andlearnCafter : set to True to prelearn M and then learn C
        """

        self.learnCreditFast(f=f,event_times=event_times,updateMconv=False,suppressC=True)
        self.updateMconv()
        self.resetC()
        self.resetF()
        self.resetftilde()
        self.resetpbase(self.counts/self.appearances)
        self.resetCounts()

        if andlearnCafter:
            self.learnCreditFast(f=f,event_times=event_times,suppressM=True,adaptlr=adaptlr,verbose=verbose)

    def makePrediction(self,delta,normalized=False,type='C'):
        """Make prediction based on current memory representation"""

        #assert(delta>=self.delta_min and delta<=self.delta_max), "Delta is "+str(delta)+" while delta max is "+str(self.delta_max)
        prefactor = self.counts/self.appearances if self.pbase is None else self.pbase
        ftilde_delta = bounded_exp(-self.s[:,np.newaxis]*delta)*self.F
        ftilde_delta = self.dkdsk.dot(ftilde_delta)
        ftilde_delta = (self.Ck[:,np.newaxis]*ftilde_delta)
        ftilde_delta[ftilde_delta<0] = 1e-20

        if type=='C':
            ftilde_delta = ftilde_delta[self.delta_start:self.delta_end,:]
            ftilde_delta *= self.deltameasure[:,np.newaxis]
            prediction = prefactor*bounded_exp(np.einsum('tij,ti->j',bounded_log(self.expC+1e-20),ftilde_delta))
        elif type=='M':
            ftilde_delta = ftilde_delta[self.k:-self.k,:]
            ftilde_delta *= self.taustarmeasure[:,np.newaxis]
            prediction = prefactor*bounded_exp(np.einsum('tij,ti->j',bounded_log(self.Mnorm+1e-20),ftilde_delta))

        if normalized:
            prediction = prediction/np.sum(prediction)

        return prediction

    def predictStream(self,normalized=False):
        """Make prediction based on current memory representation"""

        prediction = np.empty((self.num_deltas,self.num_stimuli))

        for ind,delta in enumerate(self.deltas):
            prediction[ind,:] = self.makePrediction(delta,normalized=normalized)

        return prediction


    def predictOnSequence(self,f,event_times=None,withtemporal=False,
                          refreshMemory=True,withMprediction=False,doublecount=False,
                          timesteps=None,verbose=0,returnPredictions=False):
        """
        Runs the system on input function f and makes predictions, optimized for when f is a stream of impulse functions.

        Inputs:
        f : 2D array with dim=(num. stimuli,num. timesteps). Interval is assumed
               to be dt, i.e. it is function evaluated at n*dt for n s.t. 0 < n*dt < T.
               OR
            2D array with dim=(num. stimuli,num. events), i.e. an array of all non-zero stimuli events.
        event_times : 1D array, dim=(num.events,). Array of event times.
        timesteps : int, no. of timesteps to run system for. If None, assumes whole input sequence.
        verbose : if non-zero, prints statement after every verbose events
        """

        assert(len(f.shape)==2)
        assert(f.shape[0]==self.num_stimuli)

        if withMprediction:
            if doublecount:
                print("Using M with double counting")
            else:
                print("Using M with most recent event")


        # Allows for sparse representation of f
        if event_times is not None:
            assert(len(event_times.shape)==1)
            assert(f.shape[1]==event_times.shape[0])
            f_sparse = True
            if timesteps is None: timesteps = event_times[-1]
        else:
            f_sparse = False
            event_times = np.unique(np.where(f>0)[1])
            if timesteps is None:
                timesteps = f.shape[1]
            event_times = np.append(event_times,timesteps-1)
            final_time = f.shape[1]*self.dt

        num_events = event_times.shape[0]
        numCorrect = 0
        numCorrectM = 0
        numCorrectBaseline = 0
        perplexity = 0
        perplexityM = 0
        perplexityBaseline = 0
        avgTimeError = 0

        if returnPredictions:
            storedPredictions = np.empty((num_events-1,self.num_stimuli))
            if withMprediction:
                storedPredictionsM = np.empty((num_events-1,self.num_stimuli))

        if refreshMemory:
            self.resetF()


        for i in range(num_events-1):

            if f_sparse:
                ind = i
            else:
                ind = event_times[i]

            time = event_times[i]
            time_gap = event_times[i] - event_times[i-1] if i>0 else event_times[i]

            if time>timesteps: break
            if verbose>0 and i%verbose==0: print("Reached event: ",i,"/",num_events)

            if time_gap>0:
                self.propagateF(time=(time_gap-1)*self.dt)

            self.propagateF(f[:,ind])
            self.invertF()

            if withtemporal:
                predictionWithDelta = self.predictStream()
                nextEventTime = (event_times[i+1]-event_times[i])*self.dt
                prediction = self.makePrediction(delta=nextEventTime)
                #predictionWithDelta = (predictionWithDelta>prediction[np.newaxis,:])
                predictionWithDelta = (predictionWithDelta>prediction)

                #timeError = np.sum(predictionWithDelta*self.deltameasure[:,np.newaxis])
                #avgTimeError += np.mean(timeError)
                timeError = np.sum(predictionWithDelta[:,np.argmax(f[:,ind+1])]*self.deltameasure)
                avgTimeError += timeError


                #prediction = np.sum(predictionWithDelta*self.deltameasure[:,np.newaxis],axis=0)
            else:
                nextEventTime = (event_times[i+1]-event_times[i])*self.dt
                prediction = self.makePrediction(delta=nextEventTime)

                if withMprediction:

                    if doublecount:
                        predictionM = self.makePrediction(delta=nextEventTime,type='M')
                    else:
                        ftildedelta = (self.Ck[:]*self.dkdsk.dot(bounded_exp(-self.s*nextEventTime)))[self.k:-self.k]
                        ftildedelta[ftildedelta<0] = 1e-20
                        temp = bounded_log(self.Mnorm[:,np.argmax(f[:,ind]),:])*ftildedelta[:,np.newaxis]
                        predictionM = self.kappa*bounded_exp(np.trapz(y=temp,x=self.taustars[self.k:-self.k],axis=0))


#                     if nextEventTime<=self.deltas[0]:
#                         predictionM = self.Mconv[0,np.argmax(f[:,ind]),:]
#                     elif nextEventTime>=self.deltas[-1]:
#                         predictionM = self.Mconv[-1,np.argmax(f[:,ind]),:]
#                     else:
#                         for delta_ind,delta in enumerate(self.deltas):
#                             if nextEventTime>=delta:
#                                 intervalTotal = self.deltas[delta_ind+1] - self.deltas[delta_ind]
#                                 predictionM = (nextEventTime/intervalTotal)*self.Mconv[delta_ind,np.argmax(f[:,ind]),:]
#                                 predictionM += ((intervalTotal-nextEventTime)/intervalTotal)*self.Mconv[delta_ind+1,np.argmax(f[:,ind]),:]
#                                 break


            if returnPredictions:
                storedPredictions[i,:] = prediction
                if withMprediction:
                    storedPredictionsM[i,:] = predictionM


            if np.argmax(prediction)==np.argmax(f[:,ind+1]):
                numCorrect += 1

            if np.argmax(predictionM)==np.argmax(f[:,ind+1]):
                numCorrectM += 1

            if np.argmax(self.pbase)==np.argmax(f[:,ind+1]):
                numCorrectBaseline += 1


            perplexity += -np.log2((prediction[np.argmax(f[:,ind+1])] + 1e-20)/(np.sum(prediction) + 1e-20))
            perplexityM += -np.log2((predictionM[np.argmax(f[:,ind+1])] + 1e-20)/(np.sum(predictionM) + 1e-20))
            perplexityBaseline += -np.log2((self.pbase[np.argmax(f[:,ind+1])] + 1e-20)/(np.sum(self.pbase) + 1e-20))



            #print("True Event : ",np.argmax(f[:,ind+1]))
            #print("C prediction : ",np.argmax(prediction))
            #print("M prediction : ",np.argmax(predictionM))


        accuracy = numCorrect/(num_events-1)
        accuracyM = numCorrectM/(num_events-1)
        accuracyBaseline = numCorrectBaseline/(num_events-1)
        perplexity = 2**(perplexity/(num_events-1))
        perplexityM = 2**(perplexityM/(num_events-1))
        perplexityBaseline = 2**(perplexityBaseline/(num_events-1))


        avgTimeError = avgTimeError/(num_events-1)
        print("Accuracy C : ",accuracy )
        print("Accuracy M : ",accuracyM )
        print("Baseline probability : ",accuracyBaseline)
        if withtemporal:
            print("Avg. Time Error : ",avgTimeError)


        if returnPredictions:
            if withMprediction:
                return storedPredictions, storedPredictionsM, accuracy, accuracyM, perplexity, perplexityM
            else:
                return storedPredictions, accuracy, perplexity
        else:
            if withMprediction:
                return accuracy, accuracyM, accuracyBaseline, perplexity, perplexityM, perplexityBaseline
            else:
                return accuracy, perplexity




    def estimatePerplexityFast(self,delta,f,event_times=None,verbose=False):
        """
        Runs the system on input function f and estimates perplexity using learnt C, optimized for when f is a stream of impulse functions.
        Inputs:
        f : 2D array with dim=(num. stimuli,num. timesteps). Interval is assumed
               to be dt, i.e. it is function evaluated at n*dt for n s.t. 0 < n*dt < T.
               OR
            2D array with dim=(num. stimuli,num. events), i.e. an array of all non-zero stimuli events.
        event_times : 1D array, dim=(num.events,). Array of event times.
        timesteps : int, no. of timesteps to run system for. If None, assumes whole input sequence.
        """

        assert(len(f.shape)==2)
        assert(f.shape[0]==self.num_stimuli)
        assert(delta<=self.delta_max and delta>=self.delta_min)
        assert(len(event_times.shape)==1)
        assert(f.shape[1]==event_times.shape[0])

        delta_timesteps = int(delta/self.dt)
        timesteps = event_times[-1]
        num_events = event_times.shape[0]
        propagation_times = event_times - delta_timesteps

        #print("Propagation times = ",propagation_times)

        iprop = 0
        ievent = 0
        self.resetF()
        self.resetftilde()
        time = propagation_times[0] #the lowest time

        perplexity = 0

        while iprop<num_events:

            if propagation_times[iprop] < event_times[ievent]:
                next_time = propagation_times[iprop]
                time_gap = next_time - time
                self.propagateF(time=time_gap*self.dt)
                presented_stimulus = np.argwhere(f[:,iprop]!=0)[0,0]

                prediction = self.makePrediction(delta,normalized=True)
                if verbose:
                    print("prop time ",next_time)
                    print("Propagating to time delta prior to stimulus ",presented_stimulus)
                    print("Prediction = ",prediction)
                perplexity += -np.log2(prediction[presented_stimulus])
                iprop += 1

            else:
                next_time = event_times[ievent]
                #print("event time ",next_time)
                time_gap = next_time - time
                self.propagateF(time=(time_gap-1)*self.dt)
                self.propagateF(f=f[:,ievent])
                ievent += 1

            time = next_time

        perplexity = 2**(perplexity/num_events)

        return perplexity



    def plotCredit(self,start=0,end=None,past_stimuli_ind=None,
                   future_stimuli_ind=None,stimuli_labels=None,process_labels=None,
                   colours=None,scalebymax=False,savefig=False,prefix="",suffix="",log=False):
        """
        Plots credit for desired stimulus as a function of taustar
        Inputs:
        start : index of minimum taustar to be plotted
        end : index of maximum taustar to be plotted
        preceding_stimuli : list/array with indices of stimuli to be plotted
        stimuli_labels : array of stimuli names
        """
        if end is None:
            end = self.num_deltas

        if past_stimuli_ind is None:
            past_stimuli_ind = np.arange(self.num_stimuli)

        if future_stimuli_ind is None:
            future_stimuli_ind = np.arange(self.num_stimuli)

        if stimuli_labels is None:
            stimuli_labels = self.stimuli_labels

        if process_labels is None:
            process_labels = np.zeros(self.num_stimuli,dtype=int)

        if colours is None:
            colours = [None for i in range(self.num_stimuli)]


        for i in past_stimuli_ind:
            plt.figure()
            if scalebymax:
                plt.ylim(0,1.05*np.max(self.expC))
            past_stim = str(stimuli_labels[i])
            plt.title(r"Credit for stimulus "+past_stim)
            for j in future_stimuli_ind:
                future_stim = str(stimuli_labels[j])
                lt = 'solid' if process_labels[i]==process_labels[j] else 'dashed'
                alpha = 1.0 if process_labels[i]==process_labels[j] else 0.5
                
                if log:
                    plt.plot(self.getDeltas()[start:end],np.log(self.expC[start:end,i,j]),c=colours[j],label=past_stim+"-->"+future_stim,linestyle=lt,alpha=alpha)
                else:
                    plt.plot(self.getDeltas()[start:end],self.expC[start:end,i,j],c=colours[j],label=past_stim+"-->"+future_stim,linestyle=lt,alpha=alpha)
                    

            if self.num_stimuli>=10:
                plt.legend(bbox_to_anchor=(1.01, 1.01), loc='upper left')
                if savefig:
                    plt.savefig(prefix+"credit_"+past_stim+suffix+".png",bbox_inches='tight')#,layout='tight')
            else:
                plt.legend()
                if savefig:
                    plt.savefig(prefix+"credit_"+past_stim+suffix+".png")


    def plotM(self,start=0,end=None,conv=True,past_stimuli_ind=None,
              future_stimuli_ind=None,stimuli_labels=None,process_labels=None,
              colours=None,scalebymax=False,savefig=False,prefix="",suffix=""):
        """
        Plots M for desired stimulus as a function of taustar
        Inputs:
        start : index of minimum taustar to be plotted
        end : index of maximum taustar to be plotted
        preceding_stimuli : list/array with indices of stimuli to be plotted
        stimuli_labels : array of stimuli names
        """

        if conv:
            tensor = self.Mconv
        else:
            tensor = self.Mnorm[self.delta_start-self.k:self.delta_end-self.k]


        if end is None:
            end = self.num_deltas

        if past_stimuli_ind is None:
            past_stimuli_ind = np.arange(self.num_stimuli)

        if future_stimuli_ind is None:
            future_stimuli_ind = np.arange(self.num_stimuli)

        if stimuli_labels is None:
            stimuli_labels = self.stimuli_labels

        if process_labels is None:
            process_labels = np.zeros(self.num_stimuli,dtype=int)

        if colours is None:
            colours = [None for i in range(self.num_stimuli)]


        for i in past_stimuli_ind:
            plt.figure()
            if scalebymax:
                plt.ylim(0,1.05*np.max(tensor))
            past_stim = str(stimuli_labels[i])
            plt.title(r"M for stimulus "+past_stim)
            for j in future_stimuli_ind:
                future_stim = str(stimuli_labels[j])
                lt = 'solid' if process_labels[i]==process_labels[j] else 'dashed'
                alpha = 1.0 if process_labels[i]==process_labels[j] else 0.5
                plt.plot(self.getDeltas()[start:end],tensor[start:end,i,j],c=colours[j],label=past_stim+"-->"+future_stim,linestyle=lt,alpha=alpha)

            if self.num_stimuli>=10:
                plt.legend(bbox_to_anchor=(1.01, 1.01), loc='upper left')
                if savefig:
                    plt.savefig(prefix+"M_"+past_stim+suffix+".png",bbox_inches='tight')#,layout='tight')
            else:
                plt.legend()
                if savefig:
                    plt.savefig(prefix+"M_"+past_stim+suffix+".png")

    def graphCredit(self,CorM='C',taustar_inds=None,threshold=None,
                    thresh_pct=0.75,stimuli_labels=None,colours=None,usearea=True,
                    thickness_factor=3,font_color='white',figsize=None,savefig=False,prefix="",suffix=""):

        if CorM=='C':
            tensor = self.expC
        elif CorM=='M':
            tensor = self.Mconv

        if stimuli_labels is None:
            stimuli_labels = self.stimuli_labels

        if colours is not None:
            colours = colours[:self.num_stimuli]

        label_dict = dict(zip(np.arange(self.num_stimuli,dtype=int),stimuli_labels))

        if taustar_inds is None:

            edge_labels = {}
            widths = []

            auc = np.trapz(y=tensor,x=self.deltas,axis=0)

            if threshold is None:
                if usearea:
                    threshold0 = thresh_pct*np.max(auc)
                else:
                    threshold0 = thresh_pct*np.max(tensor)
            else:
                threshold0 = threshold

            G = nx.DiGraph()
            G.add_nodes_from(np.arange(self.num_stimuli,dtype=int))


            for i in range(self.num_stimuli):
                for j in range(self.num_stimuli):

                    if usearea:
                        if auc[i,j]>=threshold0:
                            peak_taustar = self.deltas[np.argmax(tensor[:,i,j])]
                            G.add_edge(i, j, weight=peak_taustar)
                            edge_labels[(i,j)] = np.around(peak_taustar,1)
                            width = np.trapz(y=tensor[:,i,j],x=self.deltas)
                            widths.append(width)
                    else:
                        if np.max(tensor[:,i,j])>=threshold0:
                            peak_taustar = self.deltas[np.argmax(tensor[:,i,j])]
                            G.add_edge(i, j, weight=peak_taustar)
                            edge_labels[(i,j)] = np.around(peak_taustar,1)
                            width = np.max(tensor[:,i,j])/np.max(tensor[:,:,:])
                            widths.append(width)

            widths = np.array(widths)
            widths = thickness_factor*(widths/np.max(widths))
            print("Number of edges = ",len(widths))

            plt.figure(figsize=figsize)
            plt.axis('off')

            if usearea:
                plt.title(" Min. AUC = "+str(np.around(threshold0,2)))
            else:
                if CorM=='C':
                    plt.title(r"$\exp C_{threshold}$ = "+str(np.around(threshold0,2)))
                elif CorM=='M':
                    plt.title(r"$M_{threshold}$ = "+str(np.around(threshold0,2)))

            nx.draw_networkx(G, pos=nx.circular_layout(G),with_labels=True, font_weight='bold',node_color=colours,labels=label_dict,font_color=font_color)
            nx.draw_networkx_edge_labels(G, edge_labels=edge_labels,pos=nx.circular_layout(G))
            nx.draw_networkx_edges(G, pos=nx.circular_layout(G),width=widths)

            if savefig:
                plt.savefig(prefix+"creditgraph"+CorM+suffix+".png")

            plt.show()

        else:

            for i,taustar_ind in enumerate(taustar_inds):
                plt.figure(figsize=figsize)


                if threshold is None:
                    threshold0 = thresh_pct*np.max(tensor[taustar_ind,:,:])
                else:
                    threshold0 = threshold

                G = nx.DiGraph()
                taustar = self.deltas[taustar_ind]
                G.add_nodes_from(np.arange(self.num_stimuli,dtype=int))

                edges = []
                for i in range(self.num_stimuli):
                    for j in range(self.num_stimuli):
                        if tensor[taustar_ind,i,j]>=threshold0:
                            edges.append((i,j))


                G.add_edges_from(edges)
                plt.axis('off')
                if CorM=='C':
                    plt.title(r"$\tau^* = $"+str(np.around(taustar,2))+r", $\exp C_{threshold}$ = "+str(np.around(threshold0,2)))
                elif CorM=='M':
                    plt.title(r"$\tau^* = $"+str(np.around(taustar,2))+r", $M_{threshold}$ = "+str(np.around(threshold0,2)))
                nx.draw_networkx(G, pos=nx.circular_layout(G),with_labels=True, font_weight='bold',node_color=colours,labels=label_dict,font_color=font_color)

                if savefig:
                    plt.savefig(prefix+"creditgraph_"+CorM+str(taustar_ind)+suffix+".png")

            plt.show()

# Leaky Integrator Credit Learner

Learns the credit density tensor C using a leaky integrator representation of memory F and the inverse Laplace transform.
The code runs on arbitrary stimulus functions, though significant speed-ups are obtainable for delta function inputs.

## Main files

creditlearning.py contains the CreditLearner class that implements the credit-based prediction algorithm.
GraphDataset.py contains the class that implements a random walker on a random graph.
Experiment.py contains a class that can generate random continuous-time processes and mixtures of such processes

## Tutorials
Contains notebooks that introduce functionality of the CreditLearner class

## Experiments
Contains notebooks with experiments that run the credit learning algorithm on various kinds of processes

## To run notebooks
1. Clone repo and create a virtual environment from requirements.txt
2. Run notebooks as normal

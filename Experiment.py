import numpy as np
from scipy.special import factorial
import matplotlib.pyplot as plt
import networkx as nx


class Experiment:
    """
    Generates stream of stimulus impulses for given experiment time.
    Parameters that can be varied are number of stimuli, times between different stimuli,
    times between presentations, presentation probabilities.
    """

    def __init__(self,num_stimuli,dt,experiment_time):
        """
        num_stimuli : int, number of stimuli
        dt : float, minimum time interval
        experiment_time : float, total time of experiment
        """

        self.num_stimuli = num_stimuli
        self.dt = dt
        self.experiment_time = experiment_time
        self.timesteps = int(self.experiment_time/self.dt)

        self.event_times = np.array([],dtype=float)
        self.event_times_ind = np.array([],dtype=int)
        self.events = np.array([],dtype=float)

    def makeSingleProcess(self,stimuli_lags,pivot_separation,presentation_mask=None):
        """
        stimuli_lags : function, returns 1D float array, dim=no. stimuli, lags between time pivot and each stimulus,
                       should be nonnegative and pivot entry is assumed to be 0
        pivot_separation : function, returns nonnegative float corresponding to separation between time pivots
        presentation_mask : function, returns random boolean array, where only elements that are true are
        presented for the corresponding instantiation
        Example: A B C _ _ _ _ A B C _ _ _ _ , then lags = [0,1,2], pivot_separation = f : f(x) = 7
        """

        time = 0

        if presentation_mask is None:
            presentation_mask = lambda: np.ones(self.num_stimuli)

        while time<=self.experiment_time:
            current_lags = stimuli_lags()
            assert(np.all(current_lags>=0))

            mask = presentation_mask()

            for i in range(self.num_stimuli):
                if mask[i]:
                    self.event_times = np.concatenate((self.event_times,[time+current_lags[i]]))
                    event = np.zeros(self.num_stimuli)
                    event[i] = 1/self.dt

                    if len(self.events)==0:
                        self.events = event
                    else:
                        self.events = np.vstack((self.events,event))

            del_time = pivot_separation()
            assert(del_time>=0)
            time += del_time

        self.events = self.events.T
        sorting_order = self.event_times.argsort()
        self.event_times = self.event_times[sorting_order]
        self.events = self.events[:,sorting_order]
        self.event_times_ind = (self.event_times/self.dt).astype(int)

    def makeMarkovProcess(self,time_step,transitionmatrix):

        self.event_times = np.arange(0,self.experiment_time,time_step)
        self.event_times_ind = (self.event_times/self.dt).astype(int)
        self.events = np.zeros((self.num_stimuli,self.event_times.shape[0]))
        current_state = np.random.choice(self.num_stimuli)

        for i in range(len(self.event_times)):
            next_state = transitionmatrix(current_state)
            self.events[next_state,i] = 1/self.dt
            current_state = next_state


    def plotProcess(self,start=0,end=None,labels=None,colours=None,figsize=(15,1),title=None,savefig=False,directory="process.png"):
        "Generates simple plot of process"

        if end is None:
            end = len(self.event_times)

        if labels is None:
            labels = np.arange(self.num_stimuli)

        if colours is None:
            colours = [None for i in range(self.num_stimuli)]

        markers = ('o', 'v', '^', '<', '>', '8', 's', 'p', '*', 'h', 'H', 'D', 'd', 'P', 'X')

        plt.figure(figsize=figsize)
        if title is None:
            plt.title("Process up to Event "+str(end)+" of "+str(len(self.event_times)))
        else:
            plt.title(title)
        plt.yticks([])
        for i in range(self.num_stimuli):
            y = self.dt*self.events[i,start:end]
            x = self.event_times[start:end]
            plt.ylim(0.9,1.1)
            #plt.scatter(x[y!=0],y[y!=0],label=str(labels[i]),marker=markers[i])
            plt.scatter(x[y!=0],y[y!=0],s=200,label=str(labels[i]),marker='|',linewidths=10)
        plt.legend(bbox_to_anchor=(1.01, 1.01), loc='upper left')
        if savefig:
            plt.savefig(directory,bbox_inches='tight')
        plt.show()


class MixedExperiment:
    """
    Generates a stimulus stream for multiple independent processes.
    """

    def __init__(self,num_stimuli,dt,experiment_time,num_processes=None):
        """
        num_stimuli : list/array, int, number of stimuli in each process
        dt : float, minimum time interval
        experiment_time : float, total time of experiment
        num_processes : int, number of independent processes
        """

        assert(isinstance(num_stimuli,list))
        assert(len(num_stimuli)>0)
        if num_processes is not None:
            assert(num_processes==len(num_stimuli))
        else:
            self.num_processes = len(num_stimuli)

        self.num_stimuli = np.array(num_stimuli).astype(int)
        self.total_stimuli = int(np.sum(self.num_stimuli))
        self.dt = dt
        self.experiment_time = experiment_time
        self.timesteps = int(self.experiment_time/self.dt)

        self.event_times = np.array([],dtype=float)
        self.event_times_ind = np.array([],dtype=int)
        self.events = np.array([],dtype=float)

    def resetExperiment(self):
        self.event_times = np.array([],dtype=float)
        self.event_times_ind = np.array([],dtype=int)
        self.events = np.array([],dtype=float)

    def makeProcess(self,stimuli_lags,pivot_separation,presentation_mask=None):
        """
        stimuli_lags : function, args= int, process index;
                       should return 1D float array, dim=no. stimuli, lags between time pivot and
                       each stimulus for process n; values should be nonnegative; pivot lag assumed to be 0
        pivot_separation : function,args= int, process index;
                           returns nonnegative float corresponding to separation between time pivots for
                           process n
        presentation_mask : function, args= int, process index;
                            returns random boolean array, where only elements that are true are
                            presented for the corresponding instantiation for process n
        Example: A B C _ _ _ _ D E _ A B C _ , then
                 lags(0) = [0,1,2], lags(2) = [0,1], pivot_separation(0) = 10
        """

        if presentation_mask is None:
            presentation_mask = lambda n: np.ones(self.num_stimuli[n])

        for n in range(self.num_processes):
            assert(len(stimuli_lags(n))==self.num_stimuli[n])
            assert(isinstance(pivot_separation(n),float) or isinstance(pivot_separation(n),int))
            assert(len(presentation_mask(n))==self.num_stimuli[n])


        for n in range(self.num_processes):

            time = pivot_separation(n)
            assert(time>=0)
            index_shift = np.sum(self.num_stimuli[:n])

            while time<=self.experiment_time:

                current_lags = stimuli_lags(n)
                assert(np.all(current_lags>=0))

                mask = presentation_mask(n)

                for i in range(self.num_stimuli[n]):
                    if mask[i] and time+current_lags[i]<=self.experiment_time:
                        self.event_times = np.concatenate((self.event_times,[time+current_lags[i]]))
                        event = np.zeros(self.total_stimuli)
                        event[i+index_shift] = 1/self.dt

                        if len(self.events)==0:
                            self.events = event
                        else:
                            self.events = np.vstack((self.events,event))

                del_time = pivot_separation(n)
                assert(del_time>=0)
                time += del_time


        self.events = self.events.T
        sorting_order = self.event_times.argsort()
        self.event_times = self.event_times[sorting_order]
        self.events = self.events[:,sorting_order]
        self.event_times_ind = (self.event_times/self.dt).astype(int)

    def plotProcess(self,start=0,end=None,stimuli=None,labels=None,colours=None,figsize=(15,1),
                    linewidths=2,title=None,savefig=False,directory="process.png",
                    legend_anchor=(1.01, 2.01),legend_loc='upper left',ncol=1,
                    fancybox=False,edgecolor='black'):
        "Generates simple plot of process"

        if stimuli is None:
            stimuli = np.arange(self.total_stimuli)

        if end is None:
            end = len(self.event_times)

        if labels is None:
            labels = np.arange(self.total_stimuli)

        if colours is None:
            colours = [None for i in range(self.total_stimuli)]

        plt.figure(figsize=figsize)
        if title is None:
            plt.title("Process up to Event "+str(end)+" of "+str(len(self.event_times)))
        else:
            plt.title(title)
        plt.yticks([])
        for i in range(self.total_stimuli):
            y = self.dt*self.events[i,start:end]
            x = self.event_times[start:end]
            plt.ylim(0.9,1.1)
            #plt.scatter(x[y!=0],y[y!=0],label=str(labels[i]),marker=markers[i])
            plt.scatter(x[y!=0],y[y!=0],c=colours[i],s=200,label=str(labels[i]),marker='|',linewidths=linewidths)
        plt.legend(bbox_to_anchor=legend_anchor, loc=legend_loc,ncol=ncol,fancybox=fancybox,edgecolor=edgecolor)
        if savefig:
            plt.savefig(directory,bbox_inches='tight')
        plt.show()

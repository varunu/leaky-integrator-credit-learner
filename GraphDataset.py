import numpy as np
import networkx as nx
import matplotlib.pyplot as plt

"Code to generate graph dataset"

class GraphDataset:

    def __init__(self,numNodes=10,p=0.48,scale=(100,5),uniformTime=False,
                 dt=0.1,dist=lambda x,i:np.random.rand(x),uniformTransitions=True,
                 colours=None,labels=None):
        """
        Allows for simulation of random walk on random graph with normally-distributed time lags
        Inputs:
        numNodes : number of event types
        p : probability of connection between edges in random graph
        scale : tuple determining mean and variance of transition time distributions
                if uniformTime is True, scale[0] is the mean and scale[1] is the variance of every distribution
                if false, means and variances are drawn from uniform distributinos over (0,scale[0]) and (0,scale[1]) resp.
        uniformTime: modifier for scale argument
        dt : minimum time step in simulation
        dist : function taking number of connections and node index as input, returning transition prob. weights for each node
               if p=1, this can be used to specify a complete transition matrix
        uniformTransitions : if True, dist is overriden and transition probs are uniform over all connections
        colours : list of colours to associate with each node
        labels : list of labels to associate with each node
        """

        self.numNodes = numNodes
        self.p = p
        self.dt = dt

        self.connections = dict(zip([i for i in range(numNodes)],[{} for i in range(numNodes)]))
        self.edgeLabelsTimes = {}
        self.edgeLabelsProbs = {}
        self.widths = []

        if colours is None:
            self.colours = [None for i in range(self.numNodes)]
        else:
            self.colours = colours

        if labels is None:
            self.labels = np.arange(numNodes,dtype=int)
        else:
            self.labels = labels

        self.transitionProbabilities = []

        for i in range(numNodes):

            lstConnections = []
            for j in range(numNodes):
                if np.random.rand()<p:
                    lstConnections.append(j)


            if len(lstConnections)==0 or (len(lstConnections)==1 and lstConnections[0]==i):
                lstConnections.append(np.random.choice([x for x in range(numNodes) if x!=i]))

            numConnections = len(lstConnections)

            if uniformTransitions:
                transitionProbs = np.ones(numConnections)
            else:
                transitionProbs = dist(numConnections,i)

            transitionProbs = transitionProbs/np.sum(transitionProbs)
            self.transitionProbabilities.append(transitionProbs)


            if uniformTime:
                weights = scale[0]*np.ones(numConnections)
                variances = scale[1]*np.ones(numConnections)
            else:
                weights = scale[0]*np.random.rand(numConnections)
                variances = scale[1]*np.random.rand(numConnections)

            for ind,j in enumerate(lstConnections):

                self.connections[i][j] = {'weight':weights[ind],'var':variances[ind],'prob':transitionProbs[ind]}

                if i!=j:
                    self.edgeLabelsTimes[(i,j)] = str(np.around(self.connections[i][j]['weight'],1))
                    self.edgeLabelsProbs[(i,j)] = str(np.around(self.connections[i][j]['prob'],3))
                self.widths.append(np.around(self.connections[i][j]['var'],1))


#             for j in range(numNodes):
#                 if np.random.rand()<=p:
#                     self.connections[i][j] = {'weight':scale[0]*np.random.rand(),'var':scale[1]*np.random.rand()}
#                     self.edgeLabels[(i,j)] = np.around(self.connections[i][j]['weight'],1)
#                     self.widths.append(np.around(self.connections[i][j]['var'],1))
#             if len(self.connections[i].keys())==0 or (len(self.connections[i].keys())==1 and i in self.connections[i]):
#                 j = np.random.choice([x for x in range(numNodes) if x!=i])
#                 self.connections[i][j] = {'weight':scale[0]*np.random.rand(),'var':scale[1]*np.random.rand()}
#                 self.edgeLabels[(i,j)] = np.around(self.connections[i][j]['weight'],1)
#                 self.widths.append(np.around(self.connections[i][j]['var'],1))



    def printTransitionProbs(self):
        for i in self.connections:
            for j in self.connections[i]:
                print(i,'-->',j,' : ',np.around(self.connections[i][j]['prob'],3))


    def getTransitionMatrix(self,printIt=True):

        tm = np.empty((self.numNodes,self.numNodes))

        for i in range(self.numNodes):
            for j in range(self.numNodes):
                if j in self.connections[i]:
                    tm[i,j] = self.connections[i][j]['prob']
                else:
                    tm[i,j] = 0.0


        if printIt:
            print(tm)

        return tm


    def optimalAccuracy(self,start=0,end=None):

        if end is None:
            end = len(self.events)

        tm = self.getTransitionMatrix(printIt=False)
        mlChoice = np.argmax(tm,axis=1)
        numCorrect = 0

        for i in range(len(self.events)-1):
            if mlChoice[self.events[i]]==self.events[i+1]:
                numCorrect += 1

        return numCorrect/(len(self.events)-1)


    def optimalPerplexity(self,start=0,end=None):

        if end is None:
            end = len(self.events)-1

        tm = self.getTransitionMatrix(printIt=False)
        pi = np.linalg.matrix_power(tm,3000)[0]
        perplexity = 0
        perplexityUni = 0

        for i in range(start,end):
            perplexity += -np.log2(tm[self.events[i],self.events[i+1]])
            perplexityUni += -np.log2(pi[self.events[i]])


        return 2**(perplexity/(end-start)), 2**(perplexityUni/(end-start))



    def computeEntropyRate(self):

        tm = self.getTransitionMatrix(printIt=False)
        pi = np.linalg.matrix_power(tm,5000).T
        #print(pi)
        hRate = np.sum(-pi*tm*np.log2(tm+1e-20))
        return hRate



    def generateEvents(self,numSteps,cutoff=0.1,offset=0.0,indexing=None):

        self.events = [np.random.choice(self.numNodes)]
        self.times = [offset]

        if indexing is None:
            indexing = dict(zip(np.arange(self.numNodes),np.arange(self.numNodes)))

        for step in range(numSteps-1):
            nextState = np.random.choice(list(self.connections[self.events[-1]].keys()),p=self.transitionProbabilities[self.events[-1]])

            deltaTime = np.random.normal(self.connections[self.events[-1]][nextState]['weight'],self.connections[self.events[-1]][nextState]['var'])
#             if deltaTime<cutoff:
#                 deltaTime = cutoff
            
            while deltaTime<cutoff:
                deltaTime = np.random.normal(self.connections[self.events[-1]][nextState]['weight'],self.connections[self.events[-1]][nextState]['var'])
                

            self.events.append(nextState)
            self.times.append(self.times[-1]+deltaTime)

        self.events = np.array(self.events)
        self.makeOneHot()
        self.events = [indexing[i] for i in self.events]
        self.events = np.array(self.events)
        self.times = np.array(self.times)
        self.timeInds = (self.times/self.dt).astype(int)


    def plotGraph(self,figsize=(10,10),withWidths=False,withProbs=False,savefig=False,figname='trueGraph.png'):
        G = nx.DiGraph(self.connections)

        label_dict = dict(zip(np.arange(self.numNodes,dtype=int),self.labels))

        print("Number of edges = ",len(self.widths))


        if withProbs:
            edgeLabels = self.edgeLabelsProbs
        else:
            edgeLabels = self.edgeLabelsTimes


        plt.figure(figsize=figsize)
        plt.axis('off')
        nx.draw_networkx(G, pos=nx.circular_layout(G),with_labels=True, font_weight='bold',node_color=self.colours[:self.numNodes],labels=label_dict,arrowsize=20,)
        nx.draw_networkx_edge_labels(G,edge_labels=edgeLabels,pos=nx.circular_layout(G),label_pos=0.75)

        if withWidths:
            widths = self.widths
        else:
            widths = 1
        nx.draw_networkx_edges(G, pos=nx.circular_layout(G),width=widths)

        if savefig:
            plt.savefig(figname)
        plt.show()


    def makeOneHot(self):
        self.eventsOneHot = np.zeros((self.numNodes,len(self.events)))
        for i in range(len(self.events)):
            self.eventsOneHot[self.events[i],i] = 1/self.dt


    def plotEvents(self,start=1,end=None,title=None,savefig=False,figname='processstream.png',
                   figsize=(15,1),linewidths=2,
                   legend_anchor=(1.01, 2.01),legend_loc='upper left',ncol=1,
                  fancybox=False,edgecolor='black',xlim=None,legend_fontsize='medium',columnspacing=None):
        self.makeOneHot()
        start = 0
        if end is None:
            end = len(self.events)
        plt.figure(figsize=figsize)
        plt.title(title)
        plt.yticks([])
        if xlim is not None:
            plt.xlim(xlim)
        for i in range(self.numNodes):
            y = self.dt*self.eventsOneHot[i,start:end]
            x = self.times[start:end]
            plt.ylim(0.9,1.1)
            plt.scatter(x[y!=0],y[y!=0],s=200,marker='|',
                        c=self.colours[i],label=self.labels[i],linewidths=linewidths)
        plt.legend(bbox_to_anchor=legend_anchor, loc=legend_loc,ncol=ncol,fancybox=fancybox,edgecolor=edgecolor,
                  fontsize=legend_fontsize,columnspacing=columnspacing)

        if savefig:
            plt.savefig(figname,bbox_inches='tight')

        plt.show()



class MultiGraphDataset:

    def __init__(self,numNodesList,pList,scaleList,dt=0.1,uniformTime=False,dist=lambda x,i:np.random.rand(x),uniformTransitions=True,colourList=None,labelList=None):


        self.numNodesList = numNodesList
        self.numProcesses = len(self.numNodesList)

        self.totalNodes = sum(numNodesList)

        self.pList = pList
        self.scaleList = scaleList
        self.dt = dt

        if colourList is None:
            self.colourList = [[None for j in range(numNodesList[i])] for i in range(len(numNodesList))]
        else:
            self.colourList = colourList

        self.colours = []
        for j in self.colourList:
            for i in j:
                self.colours.append(i)

        if labelList is None:

            self.labelList = []
            count = 0
            for i in range(len(numNodesList)):
                lst = []
                for j in range(numNodesList[i]):
                    lst.append(count)
                    count += 1
                self.labelList.append(lst)
        else:
            self.labelList = labelList

        self.labels = []
        for j in self.labelList:
            for i in j:
                self.labels.append(i)



        self.graphs = []

        for i in range(len(numNodesList)):

            self.graphs.append(GraphDataset(numNodes=self.numNodesList[i],
                                            p=self.pList[i],scale=self.scaleList[i],
                                            dt=self.dt,dist=dist,uniformTime=uniformTime,
                                            uniformTransitions=uniformTransitions,colours=self.colourList[i],
                                            labels=self.labelList[i]))

        self.labelDict = dict(zip(np.arange(self.totalNodes),np.array(self.labelList).flatten()))



    def printTransitionProbs(self):
        for k in range(len(self.numNodesList)):
            print("Process ",k)
            for i in self.graphs[k].connections:
                for j in self.graphs[k].connections[i]:
                    print(i,'-->',j,' : ',np.around(self.graphs[k].connections[i][j]['prob'],3))

    def generateEvents(self,numStepsList,offsets=None,cutoff=0.1,trim=True,minTime=False,minSeparation=None):

        self.events = np.array([],dtype=int)
        self.times = np.array([])

        if offsets is None:
              offsets = [0 for i in range(len(self.numNodesList))]



        minEndTime = 1e30

        for i in range(len(self.numNodesList)):

            indexing = dict(zip(np.arange(self.numNodesList[i]),self.labelList[i]))
            self.graphs[i].generateEvents(numStepsList[i],indexing=indexing,cutoff=cutoff)

            endTime = self.graphs[i].times[-1]+offsets[i]

            if i==0:
                minEndtime = endTime

            if endTime<minEndTime:
                minEndTime = endTime


            self.events = np.hstack((self.events,self.graphs[i].events))
            self.times = np.hstack((self.times,self.graphs[i].times+offsets[i]))

        sorting_order = self.times.argsort()
        self.times = self.times[sorting_order]

        if minTime:

            if minSeparation is None or minSeparation<0:
                minSeparation = self.dt


            for i in range(len(self.times)-1):

                if np.abs(self.times[i+1] - self.times[i])<minSeparation:
                    self.times[i+1:] += minSeparation

        self.events = self.events[sorting_order]

        if trim:
            trimArg = np.argwhere(self.times>minEndTime)[0,0]
            self.times = self.times[:trimArg]
            self.events = self.events[:trimArg]

        self.makeOneHot()
        self.timeInds = (self.times/self.dt).astype(int)


    def makeOneHot(self):
        self.eventsOneHot = np.zeros((self.totalNodes,len(self.events)))
        for i in range(len(self.events)):
            self.eventsOneHot[self.events[i],i] = 1/self.dt


    def plotEvents(self,start=0,end=None,title=None,savefig=False,figname='processstream.png',
                  figsize=(15,1),linewidths=2,
                   legend_anchor=(1.01, 2.01),legend_loc='upper left',ncol=1,
                  fancybox=False,edgecolor='black',xlim=None,legend_fontsize='medium',columnspacing=None):
        self.makeOneHot()
        if end is None:
            end = len(self.events)
        plt.figure(figsize=figsize)
        plt.title(title)
        plt.yticks([])
        if xlim is not None:
            plt.xlim(xlim)
        for i in range(self.totalNodes):
            y = self.dt*self.eventsOneHot[i,start:end]
            x = self.times[start:end]
            plt.ylim(0.9,1.1)
            plt.scatter(x[y!=0],y[y!=0],s=200,marker='|',c=self.colours[i],label=self.labels[i],linewidths=linewidths)
        plt.legend(bbox_to_anchor=legend_anchor, loc=legend_loc,ncol=ncol,fancybox=fancybox,edgecolor=edgecolor,fontsize=legend_fontsize,columnspacing=columnspacing)

        if savefig:
            plt.savefig(figname,bbox_inches='tight')

        plt.show()

    def plotGraphs(self,figsize=(10,10),withProbs=False):


        for i,graph in enumerate(self.graphs):
            print("Graph "+str(i))
            graph.plotGraph(figsize,withProbs=withProbs)


    def plotComposedGraph(self,figsize=(10,10),withProbs=False,title=None,savefig=False,figname='processgraph.png'):

        final = nx.DiGraph(self.graphs[0].connections)
        edgeLabelsTimes = self.graphs[0].edgeLabelsTimes
        edgeLabelsProbs = self.graphs[0].edgeLabelsProbs

        for i in range(self.numProcesses-1):
            #nx.compose(final,self.graphs[i])
            mapping = dict(zip(np.arange(self.numNodesList[i+1]),self.labelList[i+1]))

            for tu in self.graphs[i+1].edgeLabelsTimes:
                edgeLabelsTimes[(mapping[tu[0]],mapping[tu[1]])] = self.graphs[i+1].edgeLabelsTimes[tu]
                edgeLabelsProbs[(mapping[tu[0]],mapping[tu[1]])] = self.graphs[i+1].edgeLabelsProbs[tu]

            H = nx.relabel_nodes(nx.DiGraph(self.graphs[i+1].connections), mapping)
            final = nx.compose(final,H)


        plt.figure(figsize=figsize)
        plt.title(title)
        plt.axis('off')
        if np.all(np.array(self.colourList)==None):
            nx.draw_networkx(final, pos=nx.circular_layout(final),with_labels=True, font_weight='bold',
                             arrowsize=12,
                             labels=self.labelDict,font_color='white')
        else:
            nx.draw_networkx(final, pos=nx.circular_layout(final),with_labels=True, font_weight='bold',
                             arrowsize=12,node_color=np.array(self.colourList).flatten(),
                             labels=self.labelDict,font_color='white')


        if withProbs:
            edgeLabels = edgeLabelsProbs
        else:
            edgeLabels = edgeLabelsTimes

        nx.draw_networkx_edge_labels(final,pos=nx.circular_layout(final),edge_labels=edgeLabels,label_pos=0.35)

#         if withWidths:
#             widths = self.widths
#         else:
#             widths = 1
        nx.draw_networkx_edges(final, pos=nx.circular_layout(final))#,width=widths)

        if savefig:
            plt.savefig(figname)

        plt.show()
